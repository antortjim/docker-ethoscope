#! /bin/bash

echo "Creating ethoscope-net network"
docker network create ethoscope-net

echo "Running node container"
docker run -d -p 32000:80 --rm  --net ethoscope-net --entrypoint /root/start_node.sh --expose 80 --volume `pwd`/results:/ethoscope_data/results --name node  antortjim/node
