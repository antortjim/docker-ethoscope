#! /bin/bash

USER_NAME=ethoscope
PASSWORD=ethoscope
NAME="ethoscope_db"
MYSQL_PORT=3306
ETHOSCOPE_ID=`cat /etc/ethoscope_id`
echo $ETHOSCOPE_ID

# remove old database
mysql -h mysqld_$ETHOSCOPE_ID -proot -P $MYSQL_PORT -u root -e "RESET MASTER";
mysql -h mysqld_$ETHOSCOPE_ID -proot -P $MYSQL_PORT -u root -e "DROP DATABASE IF EXISTS $NAME";

# create new one
mysql -h mysqld_$ETHOSCOPE_ID -proot -P $MYSQL_PORT -u root -e "CREATE USER \"$USER_NAME\"@'localhost' IDENTIFIED BY \"$PASSWORD\""
mysql -h mysqld_$ETHOSCOPE_ID -proot -P $MYSQL_PORT -u root -e "CREATE USER \"$USER_NAME\"@'%' IDENTIFIED BY \"$PASSWORD\""
mysql -h mysqld_$ETHOSCOPE_ID -proot -P $MYSQL_PORT -u root -e "GRANT ALL PRIVILEGES ON *.* TO \"$USER_NAME\"@'localhost' WITH GRANT OPTION";
mysql -h mysqld_$ETHOSCOPE_ID -proot -P $MYSQL_PORT -u root -e "GRANT ALL PRIVILEGES ON *.* TO \"$USER_NAME\"@'%' WITH GRANT OPTION";

## Identity
cd /opt/ethoscope-git/src/scripts/
/usr/bin/python3 /opt/ethoscope-git/src/scripts/device_server.py -D
