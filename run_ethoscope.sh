#! /bin/bash

ETHOSCOPE_NUMBER=$1
SRC_VIDEO=$2

echo $ETHOSCOPE_NUMBER
echo "Running mysql container with user and password root and name mysqld"
docker run -d -P --rm --name mysqld_${ETHOSCOPE_NUMBER}e27AB5a9e19f94de287e28f789825 --net ethoscope-net -e MYSQL_ROOT_PASSWORD="root" mysql:5.7  && sleep 30
echo "Running ethoscope container"
docker run -dP --rm  --net ethoscope-net --name ETHOSCOPE_$ETHOSCOPE_NUMBER --volume "$SRC_VIDEO":/ethoscope_data/upload/video antortjim/ethoscope_${ETHOSCOPE_NUMBER}

